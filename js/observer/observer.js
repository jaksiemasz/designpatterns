define(function () {
    'use strict';

    var Observer = function (name) {
        this.name = name;

    };

    Observer.prototype.notify = function (event, data) {
        console.log('event :'+ event + ' data: '+ data + ' name: ' + this.name);

    };

    return Observer;

});