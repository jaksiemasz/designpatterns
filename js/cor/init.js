define(function (require){
    'use strict';

    return {
        init: function () {
            var call, sms, email, handler,
                Handler = require('cor/handler'),
                callHandler = require('cor/handlers/callHandler');

            call = {
                type: 'call',
                number: '123456789',
                ownNumber: '987654321'
            };

            sms = {
                type: 'sms',
                number: '5846468',
                message: 'hello from sms'
            };

            email = {
                type: 'email',
                recipient: 'receipient@sduhysgv.com',
                message: 'hi from my email'
            };

            handler = new Handler(null, null, callHandler);

            handler.handleCommunication(email)
            handler.handleCommunication(sms)
        }
    }
});
