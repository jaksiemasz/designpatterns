define(function (require) {
    'use strict';

    var Handler = require('cor/handler'),
        emailHandler = require('cor/handlers/emailHandler'),
        smsHandler;

    smsHandler = new Handler('sms', handleSms, emailHandler);

    function handleSms(sms) {
        console.log('handling sms', sms.number, 'message', sms.message);
    }

    return smsHandler;
});